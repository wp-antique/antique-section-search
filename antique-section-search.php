<?php
/**
 * Plugin Name: Antique Section Search
 * Plugin URI: https://gitlab.com/wp-antique/plugins/antique-section-search
 * Description: Add a search field to your pages that allows visitors to quickly jump to a specified section.
 * Version: 1.0.0
 * Author: Simon Garbin
 * Author URI: https://simongarbin.com
 * License: GPLv3
 * License URI: https://www.gnu.org/licenses/gpl-3.0.html.en
 * Text Domain: antique-section-search
 * Domain Path: /languages
 */
/*
  Copyright 2023 Simon Garbin
  Antique Section Search is free software: you can redistribute it and/or
  modify it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  any later version.

  Antique Section Search is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Antique Section Search. If not, see
  https://www.gnu.org/licenses/gpl-3.0.html.en.
 */
/*
  Comments:
  - Every function is prefixed by the plugin name in order to prevent
  name collisions.
  - In almost all functions named arguments are used in order to facilitate
  the understanding of the code.
  - Single quotation marks are used for php/js code, double quotation marks
  for HTML.
 */

include plugin_dir_path(__FILE__) . '/shared/menu.php';
include plugin_dir_path(__FILE__) . '/shared/register.php';
include plugin_dir_path(__FILE__) . '/settings.php';

defined('ABSPATH') || exit;

/*
  ----------------------------------------
  Translation
  ----------------------------------------
 */

add_action(
        hook_name: 'init',
        callback: 'antique_section_search_load_textdomain'
);

function antique_section_search_load_textdomain() {
    load_plugin_textdomain(
            domain: 'antique-section-search',
            deprecated: false,
            plugin_rel_path: dirname(plugin_basename(__FILE__)) . '/languages'
    );
}

function antique_section_search_translation_dummy() {
    $plugin_description = __('Add a search field to your pages that allows '
            . 'visitors to quickly jump to a specified section.',
            'antique-section-search');
}

/*
  ----------------------------------------
  Admin Settings
  ----------------------------------------
 */

add_filter(
        hook_name: 'plugin_action_links_' . plugin_basename(__FILE__),
        callback: 'antique_section_search_settings_link'
);

function antique_section_search_settings_link(array $links) {

    $url = admin_url(path: 'admin.php?page=antique_section_search');
    $link = '<a href="' . esc_html($url) . '">'
            . __('Settings', 'antique-section-search')
            . '</a>';
    $links[] = $link;

    return $links;
}

/*
  ----------------------------------------
  Styles and Scripts
  ----------------------------------------
 */

add_action(
        hook_name: 'wp_head',
        callback: 'antique_section_search_custom_style'
);

add_action(
        hook_name: 'enqueue_block_editor_assets',
        callback: 'antique_section_search_custom_style'
);

function antique_section_search_custom_style() {

    $options = get_antique_section_search_options();

    $heading_has_border = $options['heading_border_bool'] == 'on';
    $content_has_border = $options['content_border_bool'] == 'on';
    $is_antique = $options['antique_style'] == 'on' && wp_get_theme() == 'Antique';

    $heading_bg_color = $is_antique ? 'var(--antique-blocks--heading--bg-color)' :
            $options['heading_bg_color'];
    $heading_font_color = $is_antique ? 'var(--antique-blocks--heading--font-color)' :
            $options['heading_font_color'];
    $heading_border_color = $is_antique ? 'var(--antique-blocks--heading--border-color)' :
            $options['heading_border_color'];
    $heading_border = $heading_has_border ? sprintf(
                    '%spx %s %s',
                    $options['heading_border_width'],
                    $options['heading_border_style'],
                    $heading_border_color
            ) : 'none';

    $content_bg_color = $is_antique ? 'var(--antique-blocks--content--bg-color)' :
            $options['content_bg_color'];
    $content_font_color = $is_antique ? 'var(--antique-blocks--content--font-color)' :
            $options['content_font_color'];
    $content_border_color = $is_antique ? 'var(--antique-blocks--content--border-color)' :
            $options['content_border_color'];
    $content_border = $content_has_border ? sprintf(
                    '%spx %s %s',
                    $options['content_border_width'],
                    $options['content_border_style'],
                    $content_border_color
            ) : 'none';

    $style = sprintf(
            '<style id="antique-section-search-custom-css">'
            . ':root {'
            . '--antique-section-search--heading--bg-color: %s;'
            . '--antique-section-search--heading--font-color: %s;'
            . '--antique-section-search--heading--border: %s;'
            . '--antique-section-search--content--bg-color: %s;'
            . '--antique-section-search--content--font-color: %s;'
            . '--antique-section-search--content--border: %s;'
            . '}'
            . '</style>',
            $heading_bg_color,
            $heading_font_color,
            $heading_border,
            $content_bg_color,
            $content_font_color,
            $content_border
    );

    echo $style;
}

add_action(
        hook_name: 'wp_enqueue_scripts',
        callback: 'antique_section_search_functions'
);

function antique_section_search_functions() {

    $handle = 'antique-section-search-function';

    wp_register_script(
            handle: $handle,
            src: plugins_url('/assets/js/search.js', __FILE__),
            deps: array(),
            ver: false,
            in_footer: true
    );
}

/*
  ----------------------------------------
  Section Search Block
  ----------------------------------------
 */

add_action(
        hook_name: 'init',
        callback: 'antique_section_search_register_block'
);

function antique_section_search_register_block() {

    $asset_file = include( plugin_dir_path(__FILE__) . '/block/block.asset.php');
    $script_handle = 'antique-section-search-block-script';

    wp_register_script(
            handle: $script_handle,
            src: plugins_url('block/block.js', __FILE__),
            deps: $asset_file['dependencies'],
            ver: $asset_file['version']
    );

    wp_set_script_translations(
            handle: $script_handle,
            domain: 'antique-section-search',
            path: plugin_dir_path(__FILE__) . '/languages/'
    );

    register_block_type(
            block_type: plugin_dir_path(__FILE__) . '/block/',
            args: array(
                'api_version' => 2,
                'render_callback' => 'antique_section_search_render_callback'
            )
    );
}

function antique_section_search_render_callback() {
    return get_the_antique_section_search();
}

function get_the_antique_section_search() {

    $options = get_antique_section_search_options();
    $is_closed = $options['on_large_screen'] == 'closed';

    $search = '<div class="antique-section-search antique-block">'
            . '<div class="antique-section-search-heading-wrap antique-block-heading-wrap">'
            . '<span class="antique-section-search-heading antique-block-heading">'
            . esc_html__('Sections', 'antique-section-search')
            . '</span>'
            . '<span class="antique-block-arrow antique-block-show-content'
            . ($is_closed ? ' rotate-arrow' : '')
            . '">'
            . '<i class="fa fa-angle-down"></i>'
            . '</span>'
            . '</div>';

    $search .= '<div class="antique-section-search-content antique-block-content'
            . ($is_closed ? ' hide-content' : '')
            . '">'
            . '<div class="antique-section-search-field">'
            . '<input type="text" '
            . 'class="antique-section-search-inp" '
            . 'name="antique-section-search-inp" '
            . 'placeholder="z. B. 3,1">'
            . '<button '
            . 'class="antique-section-search-btn" '
            . 'name="antique-section-search-btn">'
            . esc_html__('Search', 'antique-section-search')
            . '</button>'
            . '</div>';
    $search .= '<div class="antique-section-search-error-msg">'
            . '<span class="invalid-msg">'
            . esc_html__('Input invalid.', 'antique-section-search')
            . '</span>'
            . '<span class="not-found-msg">'
            . esc_html__('This section does not exist.', 'antique-section-search')
            . '</span>'
            . '</div>'
            . '</div>'
            . '</div>';

    return $search;
}
