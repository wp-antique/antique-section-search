(function () {

    var searchs = document.querySelectorAll('.antique-section-search');
    
    if (!searchs) {
        return;
    }

    [...searchs].forEach(function (search) {

        var input_field = search.querySelector('.antique-section-search-inp');
        var input_button = search.querySelector('.antique-section-search-btn');
        
        var error_msg_box = search.querySelector('.antique-section-search-error-msg');
        var invalid_msg = error_msg_box.querySelector('.invalid-msg');
        var not_found_msg = error_msg_box.querySelector('.not-found-msg');

        input_field.value = '';

        input_field.addEventListener('input', function (e) {
            if (input_field.value === '') {
                invalid_msg.style.display = 'none';
                not_found_msg.style.display = 'none';
            }
        });

        input_field.addEventListener('keypress', function (e) {
            if (e.key === 'Enter') {
                search_section(input_field, error_msg_box);
            }
        });

        input_button.addEventListener('click', function (e) {
            search_section(input_field, error_msg_box);
        });

    });

    function search_section(input_field, error_msg_box) {

        var invalid_msg = error_msg_box.querySelector('.invalid-msg');
        var not_found_msg = error_msg_box.querySelector('.not-found-msg');

        if (input_field.value.length !== 0) {

            var input = String(input_field.value);
            input = input.replace(/\s+/g, '');
            input = input.replace(/[.:;-]/g, ',');

            if (!is_valid(input)) {
                invalid_msg.style.display = 'block';
                not_found_msg.style.display = 'none';
                return;
            }

            input_field.value = input;

            var section = document.getElementById(input);

            if (section !== null) {
                var anchor = section.id;
                window.location = window.location.origin + window.location.pathname + '#' + anchor;

                not_found_msg.style.display = 'none';
                invalid_msg.style.display = 'none';
            } else {
                not_found_msg.style.display = 'block';
                invalid_msg.style.display = 'none';
            }
        }
    }

    function is_valid(input) {

        splitted = input.split(',');

        for (var i = 0; i < splitted.length; ++i) {
            if (isNaN(splitted[i])) {
                return false;
            }
        }

        return true;
    }

}
)($);