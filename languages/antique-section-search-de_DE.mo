��    '      T      �      �  ]   �     �     �  �        �  	   �     �     �     �                         )  %   7     ]     c     p     w     �     �     �     �  �  �     �     �     �     �     �     �     �           	                          &  �  +  |   �	     C
     S
  �   k
     c  	   u          �     �     �  
   �     �     �     �  *   �               ,     2  
   B     M     [     d  �  x  !   u  *   �  '   �     �     �                         $     *     /     <     B   Add a search field to your pages that allows visitors to quickly jump to a specified section. Antique Plugins Antique Section Search Apply the colors set within the Antique theme. This option only works if the Antique theme is installed and activated. Upon activation of another theme the colors specified above are used. Background color: Behaviour Block Title Border: Button Font color: Hide ID Input invalid. Large screen: Pages and posts containing the block: Reset Save changes Search Section Search Sections Settings Show Small screen: This plugin adds a search field that allows visitors of your website to search a section on your page. After entering the section the browser jumps to the specified anchor. When you create or edit a page, you can select the block "Antique Section Search" and insert it into the page. Additionally, if you are using the Antique theme, there is a "Sidebar" section on the right settings sidebar. There you can choose to display the search field in the sidebar of the page. This section does not exist. Your settings have been reset. Your settings have been saved. button button, sidebar closed link location open page post sidebar title type Project-Id-Version: Antique Section Search
Report-Msgid-Bugs-To: 
Last-Translator: 
Language-Team: Simon Garbin
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
POT-Creation-Date: 2023-05-26 15:16+0200
PO-Revision-Date: 2023-05-26 15:16+0200
Language: de_DE
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.0.1
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: __;_e;esc_html__;esc_html_e;esc_attr__;esc_attr_e
X-Poedit-SearchPath-0: settings.php
X-Poedit-SearchPath-1: antique-section-search.php
X-Poedit-SearchPath-2: block/block.js
X-Poedit-SearchPath-3: block/block.json
X-Poedit-SearchPath-4: shared/menu.php
 Füge ein Suchfeld in deine Seiten ein, mit welchem Besucher schnell zu einem bestimmten Abschnitt im Text springen können. Antique Plugins Antique Abschnitt-Suche Verwende die Farben, die im Antique-Theme festgelegt wurden. Diese Einstellung kann nur angewandt werden, wenn das Antique-Theme installiert und aktiviert ist. Sobald ein anderes Theme aktiviert wird, werden die oben festgelegten Farben verwendet. Hintergrundfarbe: Verhalten Block-Titel Rahmen: Button Schriftfarbe: Verstecken ID Eingabe ungültig. Großer Bildschirm: Seiten und Posts, die den Block enthalten: Zurücksetzen Änderungen speichern Suche Abschnitt-Suche Abschnitte Einstellungen Anzeigen Kleiner Bildschirm: Mit diesem Plugin kannst du auf deinen Seiten eine Abschnitt-Suche einblenden. Nach der Eingabe des Abschnitts springt der Browser zur eingegebenen Stelle. Wenn du eine Seite erstellst oder bearbeitest, wähle den Block "Antique Abschnitt-Suche" und füge ihn in deine Seite ein. Zusätzlich siehst du, falls das Antique-Theme aktiviert ist, in der rechten Seitenleiste unter den Einstellungen den Abschnitt "Seitenleiste". Dort kannst du festlegen, ob das Suchfeld in der Seitenleiste angezeigt werden soll. Dieser Abschnitt existiert nicht. Deine Einstellungen wurden zurückgesetzt. Deine Einstellungen wurden gespeichert. Button Inhalt, Seitenleiste geschlossen Link Stelle offen Seite Post Seitenleiste Titel Typ 