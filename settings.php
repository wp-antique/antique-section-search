<?php

function get_antique_section_search_default_options() {

    $defaults = array(
        'heading_font_color' => '#ffffff',
        'heading_bg_color' => '#8e8e8e',
        'heading_border_bool' => '',
        'heading_border_width' => '1',
        'heading_border_style' => 'solid',
        'heading_border_color' => '#000000',
        'content_font_color' => '#000000',
        'content_bg_color' => '#f2f2f2',
        'content_border_bool' => '',
        'content_border_width' => '1',
        'content_border_style' => 'solid',
        'content_border_color' => '#000000',
        'on_large_screen' => 'open',
        'on_small_screen' => 'closed',
        'antique_style' => '',
    );

    return $defaults;
}

function get_antique_section_search_options() {

    $defaults = get_antique_section_search_default_options();

    $options = wp_parse_args(
            args: get_option('antique_section_search_options'),
            defaults: $defaults
    );

    return $options;
}

add_action(
        hook_name: 'admin_menu',
        callback: 'antique_section_search_options_page'
);

function antique_section_search_options_page() {

    if (!function_exists('antique_plugins_admin_menu')) {
        add_menu_page(
                page_title: __('Antique Section Search', 'antique-section-search'),
                menu_title: __('Antique Section Search', 'antique-section-search'),
                capability: 'manage_options',
                menu_slug: 'antique_section_search',
                callback: 'antique_section_search_options_page_html'
        );
    } else {
        add_submenu_page(
                parent_slug: 'antique_plugins',
                page_title: __('Antique Plugins', 'antique-section-search')
                . ' &rsaquo; ' . __('Section Search', 'antique-section-search'),
                menu_title: __('Section Search', 'antique-section-search'),
                capability: 'manage_options',
                menu_slug: 'antique_section_search',
                callback: 'antique_section_search_options_page_html'
        );
    }
}

add_action(
        hook_name: 'admin_init',
        callback: 'antique_section_search_remove_menu_page'
);

function antique_section_search_remove_menu_page() {
    if (!function_exists('antique_plugins_admin_menu')) {
        remove_menu_page(menu_slug: 'antique_section_search');
    }
}

function antique_section_search_options_page_html() {

    if (!current_user_can(capability: 'manage_options')) {
        return;
    }
    ?>

    <div class="wrap antique-plugin-settings-wrap">
        <h1><?php echo esc_html(get_admin_page_title()); ?></h1>

        <?php settings_errors(setting: 'antique_section_search_updated_message'); ?>
        <?php settings_errors(setting: 'antique_section_search_reset_message'); ?>

        <p><?php
            esc_html_e('This plugin adds a search field that allows visitors '
                    . 'of your website to search a section on your page. '
                    . 'After entering the section the browser jumps to the '
                    . 'specified anchor. When you create or edit a page, you '
                    . 'can select the block "Antique Section Search" and insert '
                    . 'it into the page. Additionally, if you are using the '
                    . 'Antique theme, there is a "Sidebar" section on the '
                    . 'right settings sidebar. There you can choose to display '
                    . 'the search field in the sidebar of the page.',
                    'antique-section-search');
            ?></p>

        <form action="options.php" method="post">
            <?php
            settings_fields(option_group: 'antique_section_search_fields');
            do_settings_sections(page: 'antique_section_search');
            ?>

            <div class="antique-plugin-submit-buttons-wrap">
                <?php
                submit_button(
                        text: __('Save changes', 'antique-section-search'),
                        type: 'primary',
                        name: 'updated',
                        wrap: false
                );
                submit_button(
                        text: __('Reset', 'antique-section-search'),
                        type: 'secondary',
                        name: 'reset',
                        wrap: false
                );
                ?>
            </div>

            <?php antique_section_search_where_is_used(); ?>
        </form>
    </div>

    <?php
}

function antique_section_search_settings_sanitize_cb($input) {

    if (isset($_POST['updated'])) {

        add_settings_error(
                setting: 'antique_section_search_updated_message',
                code: 'antique_section_search_updated_message_code',
                message: __('Your settings have been saved.', 'antique-section-search'),
                type: 'updated'
        );
    } else if (isset($_POST['reset'])) {

        add_settings_error(
                setting: 'antique_section_search_reset_message',
                code: 'antique_section_search_reset_message_code',
                message: __('Your settings have been reset.', 'antique-section-search'),
                type: 'updated'
        );

        return array();
    }

    return $input;
}

add_action(
        hook_name: 'admin_init',
        callback: 'antique_section_search_settings_init'
);

function antique_section_search_settings_init() {

    $option_name = 'antique_section_search_options';

    register_setting(
            option_group: 'antique_section_search_fields',
            option_name: $option_name,
            args: array(
                'sanitize_callback' => 'antique_section_search_settings_sanitize_cb'
            )
    );

    add_settings_section(
            id: 'antique_section_search_settings_title_section',
            title: __('Block Title', 'antique-section-search'),
            callback: 'antique_section_search_settings_title_section_cb',
            page: 'antique_section_search'
    );

    add_settings_field(
            id: 'heading_font_color',
            title: __('Font color:', 'antique-section-search'),
            callback: 'antique_section_search_field_heading_font_color_cb',
            page: 'antique_section_search',
            section: 'antique_section_search_settings_title_section',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'heading_font_color',
            )
    );

    add_settings_field(
            id: 'heading_bg_color',
            title: __('Background color:', 'antique-section-search'),
            callback: 'antique_section_search_field_heading_bg_color_cb',
            page: 'antique_section_search',
            section: 'antique_section_search_settings_title_section',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'heading_bg_color',
            )
    );

    add_settings_field(
            id: 'heading_border',
            title: __('Border:', 'antique-section-search'),
            callback: 'antique_section_search_field_heading_border_cb',
            page: 'antique_section_search',
            section: 'antique_section_search_settings_title_section',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'heading_border',
            )
    );

    add_settings_section(
            id: 'antique_section_search_settings_button_section',
            title: __('Button', 'antique-section-search'),
            callback: 'antique_section_search_settings_button_section_cb',
            page: 'antique_section_search'
    );

    add_settings_field(
            id: 'content_font_color',
            title: __('Font color:', 'antique-section-search'),
            callback: 'antique_section_search_field_button_font_color_cb',
            page: 'antique_section_search',
            section: 'antique_section_search_settings_button_section',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'content_font_color',
            )
    );

    add_settings_field(
            id: 'content_bg_color',
            title: __('Background color:', 'antique-section-search'),
            callback: 'antique_section_search_field_button_bg_color_cb',
            page: 'antique_section_search',
            section: 'antique_section_search_settings_button_section',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'content_bg_color',
            )
    );

    add_settings_field(
            id: 'content_border',
            title: __('Border:', 'antique-section-search'),
            callback: 'antique_section_search_field_button_border_cb',
            page: 'antique_section_search',
            section: 'antique_section_search_settings_button_section',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'content_border',
            )
    );

    add_settings_field(
            id: 'antique_style',
            title: '',
            callback: 'antique_section_search_field_antique_style_cb',
            page: 'antique_section_search',
            section: 'antique_section_search_settings_button_section',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'antique_style',
            )
    );

    add_settings_section(
            id: 'antique_section_search_settings_behaviour_section',
            title: __('Behaviour', 'antique-section-search'),
            callback: 'antique_section_search_settings_behaviour_section_cb',
            page: 'antique_section_search'
    );

    add_settings_field(
            id: 'on_large_screen',
            title: __('Large screen:', 'antique-section-search'),
            callback: 'antique_section_search_field_on_large_screen_cb',
            page: 'antique_section_search',
            section: 'antique_section_search_settings_behaviour_section',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'on_large_screen',
            )
    );

    add_settings_field(
            id: 'on_small_screen',
            title: __('Small screen:', 'antique-section-search'),
            callback: 'antique_section_search_field_on_small_screen_cb',
            page: 'antique_section_search',
            section: 'antique_section_search_settings_behaviour_section',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'on_small_screen',
            )
    );
}

function antique_section_search_settings_title_section_cb($args) {

}

function antique_section_search_field_heading_font_color_cb($args) {

    $field_id = esc_attr($args['label_for']);

    $option_name = esc_attr($args['option_name']);
    $options = get_antique_section_search_options();
    ?>

    <input type="text"
           class="color-field"
           id="<?php echo $field_id; ?>"
           name="<?php echo $option_name; ?>[<?php echo $field_id; ?>]"
           value="<?php echo esc_attr($options[$field_id]); ?>"
           data-default-color="#ffffff"
           autocomplete="off"
           >

    <?php
}

function antique_section_search_field_heading_bg_color_cb($args) {

    $field_id = esc_attr($args['label_for']);

    $option_name = esc_attr($args['option_name']);
    $options = get_antique_section_search_options();
    ?>

    <input type="text"
           class="color-field"
           id="<?php echo $field_id; ?>"
           name="<?php echo $option_name; ?>[<?php echo $field_id; ?>]"
           value="<?php echo esc_attr($options[$field_id]); ?>"
           data-default-color="#8e8e8e"
           autocomplete="off"
           >

    <?php
}

function antique_section_search_field_heading_border_cb($args) {

    $option_name = $args['option_name'];
    $options = get_antique_section_search_options();

    $field_id = esc_attr($args['label_for']);
    $str_border_bool = esc_attr($field_id . '_bool');
    $str_border_width = esc_attr($field_id . '_width');
    $str_border_style = esc_attr($field_id . '_style');
    $str_border_color = esc_attr($field_id . '_color');
    ?>

    <div class="border-customization">

        <input type="checkbox"
               class="enable-border"
               id="<?php echo $str_border_bool; ?>"
               name="<?php echo esc_attr($option_name); ?>[<?php echo $str_border_bool; ?>]"
               <?php echo $options[$str_border_bool] != '' ? 'checked="checked"' : ''; ?>
               autocomplete="off"
               >

        <div class="border-settings">
            <input type="number"
                   id="<?php echo $str_border_width; ?>"
                   name="<?php echo esc_attr($option_name); ?>[<?php echo $str_border_width; ?>]"
                   value="<?php echo esc_attr($options[$str_border_width]); ?>"
                   min="1" max="10"
                   style="width: 60px"
                   autocomplete="off"
                   >
            <label for="<?php echo $str_border_width; ?>">px</label>

            <select id="<?php echo $str_border_style; ?>"
                    name="<?php echo esc_attr($option_name); ?>[<?php echo $str_border_style; ?>]"
                    autocomplete="off"
                    >

                <option value="solid" <?php
                echo isset($options[$str_border_style]) ?
                        ( selected($options[$str_border_style], 'solid', false) ) :
                        ( '' );
                ?>
                        >solid</option>

                <option value="double" <?php
                echo isset($options[$str_border_style]) ?
                        ( selected($options[$str_border_style], 'double', false) ) :
                        ( '' );
                ?>
                        >double</option>
            </select>

            <input type="text"
                   class="color-field"
                   id="<?php echo $str_border_color; ?>"
                   name="<?php echo esc_attr($option_name); ?>[<?php echo $str_border_color; ?>]"
                   value="<?php echo esc_attr($options[$str_border_color]); ?>"
                   data-default-color="#000000"
                   autocomplete="off"
                   >
        </div>

    </div>

    <?php
}

function antique_section_search_settings_button_section_cb($args) {

}

function antique_section_search_field_button_font_color_cb($args) {

    $field_id = esc_attr($args['label_for']);

    $option_name = esc_attr($args['option_name']);
    $options = get_antique_section_search_options();
    ?>

    <input type="text"
           class="color-field"
           id="<?php echo $field_id; ?>"
           name="<?php echo $option_name; ?>[<?php echo $field_id; ?>]"
           value="<?php echo esc_attr($options[$field_id]); ?>"
           data-default-color="#000000"
           autocomplete="off"
           >

    <?php
}

function antique_section_search_field_button_bg_color_cb($args) {

    $field_id = esc_attr($args['label_for']);

    $option_name = esc_attr($args['option_name']);
    $options = get_antique_section_search_options();
    ?>

    <input type="text"
           class="color-field"
           id="<?php echo $field_id; ?>"
           name="<?php echo $option_name; ?>[<?php echo $field_id; ?>]"
           value="<?php echo esc_attr($options[$field_id]); ?>"
           data-default-color="#f2f2f2"
           autocomplete="off"
           >

    <?php
}

function antique_section_search_field_button_border_cb($args) {

    $option_name = $args['option_name'];
    $options = get_antique_section_search_options();

    $field_id = esc_attr($args['label_for']);
    $str_border_bool = esc_attr($field_id . '_bool');
    $str_border_width = esc_attr($field_id . '_width');
    $str_border_style = esc_attr($field_id . '_style');
    $str_border_color = esc_attr($field_id . '_color');
    ?>

    <div class="border-customization">

        <input type="checkbox"
               class="enable-border"
               id="<?php echo $str_border_bool; ?>"
               name="<?php echo esc_attr($option_name); ?>[<?php echo $str_border_bool; ?>]"
               <?php echo $options[$str_border_bool] != '' ? 'checked="checked"' : ''; ?>
               autocomplete="off"
               >

        <div class="border-settings">
            <input type="number"
                   id="<?php echo $str_border_width; ?>"
                   name="<?php echo esc_attr($option_name); ?>[<?php echo $str_border_width; ?>]"
                   value="<?php echo esc_attr($options[$str_border_width]); ?>"
                   min="1" max="10"
                   style="width: 60px"
                   autocomplete="off"
                   >
            <label for="<?php echo $str_border_width; ?>">px</label>

            <select id="<?php echo $str_border_style; ?>"
                    name="<?php echo esc_attr($option_name); ?>[<?php echo $str_border_style; ?>]"
                    autocomplete="off"
                    >

                <option value="solid" <?php
                echo isset($options[$str_border_style]) ?
                        ( selected($options[$str_border_style], 'solid', false) ) :
                        ( '' );
                ?>
                        >solid</option>

                <option value="double" <?php
                echo isset($options[$str_border_style]) ?
                        ( selected($options[$str_border_style], 'double', false) ) :
                        ( '' );
                ?>
                        >double</option>
            </select>

            <input type="text"
                   class="color-field"
                   id="<?php echo $str_border_color; ?>"
                   name="<?php echo esc_attr($option_name); ?>[<?php echo $str_border_color; ?>]"
                   value="<?php echo esc_attr($options[$str_border_color]); ?>"
                   data-default-color="#000000"
                   autocomplete="off"
                   >
        </div>

    </div>

    <?php
}

function antique_section_search_field_antique_style_cb($args) {

    $field_id = esc_attr($args['label_for']);

    $option_name = esc_attr($args['option_name']);
    $options = get_antique_section_search_options();
    ?>

    <input type="checkbox"
           id="<?php echo $field_id; ?>"
           name="<?php echo $option_name; ?>[<?php echo $field_id; ?>]"
           <?php echo $options[$field_id] != '' ? 'checked="checked"' : ''; ?>
           autocomplete="off"
           >
    <label for="<?php echo $field_id; ?>"><?php
        esc_html_e('Apply the colors set within the Antique theme. This '
                . 'option only works if the Antique theme is installed and '
                . 'activated. Upon activation of another theme the colors '
                . 'specified above are used.',
                'antique-section-search');
        ?></label>

    <?php
}

function antique_section_search_settings_behaviour_section_cb($args) {

}

function antique_section_search_field_on_large_screen_cb($args) {

    $field_id = esc_attr($args['label_for']);

    $option_name = esc_attr($args['option_name']);
    $options = get_antique_section_search_options();
    ?>

    <select id="<?php echo $field_id; ?>"
            name="<?php echo esc_attr($option_name); ?>[<?php echo $field_id; ?>]"
            autocomplete="off"
            >

        <option value="open" <?php
        echo isset($options[$field_id]) ?
                ( selected($options[$field_id], 'open', false) ) :
                ( '' );
        ?>
                ><?php esc_html_e('open', 'antique-section-search'); ?></option>

        <option value="closed" <?php
        echo isset($options[$field_id]) ?
                ( selected($options[$field_id], 'closed', false) ) :
                ( '' );
        ?>
                ><?php esc_html_e('closed', 'antique-section-search'); ?></option>
    </select>

    <?php
}

function antique_section_search_field_on_small_screen_cb($args) {

    $field_id = esc_attr($args['label_for']);

    $option_name = esc_attr($args['option_name']);
    $options = get_antique_section_search_options();
    ?>

    <select id="<?php echo $field_id; ?>"
            name="<?php echo esc_attr($option_name); ?>[<?php echo $field_id; ?>]"
            autocomplete="off"
            >

        <option value="open" <?php
        echo isset($options[$field_id]) ?
                ( selected($options[$field_id], 'open', false) ) :
                ( '' );
        ?>
                ><?php esc_html_e('open', 'antique-section-search'); ?></option>

        <option value="closed" <?php
        echo isset($options[$field_id]) ?
                ( selected($options[$field_id], 'closed', false) ) :
                ( '' );
        ?>
                ><?php esc_html_e('closed', 'antique-section-search'); ?></option>
    </select>

    <?php
}


function antique_section_search_where_is_used() {

    $theme_is_antique = wp_get_theme() == 'Antique';
    $search_for = '<!-- wp:antique-section-search/block ';
    $meta_key = '_antique_theme_section_search_meta_key';

    $query = new WP_Query(array(
        'post_type' => array('post', 'page'),
        'posts_per_page' => -1
    ));

    $pages = array();

    if ($query->have_posts()) {
        while ($query->have_posts()) {

            $query->the_post();
            $id = get_the_ID();

            $is_in_content = strpos(
                    haystack: get_the_content($id),
                    needle: $search_for
            );

            if ($theme_is_antique) {

                $meta_exists = metadata_exists(
                        meta_type: 'post',
                        object_id: $id,
                        meta_key: $meta_key
                );

                if ($meta_exists) {
                    $meta = get_post_meta(
                            post_id: get_the_ID(),
                            key: $meta_key,
                            single: true
                    );
                    $meta_is_checked = $meta != '';
                } else {
                    $meta_is_checked = false;
                }
            }

            $post_type_en = get_post_type();
            if ($post_type_en == 'page') {
                $post_type = esc_html__('page', 'antique-section-search');
            } else if ($post_type_en == 'post') {
                $post_type = esc_html__('post', 'antique-section-search');
            } else {
                $post_type = $post_type_en;
            }

            if ($is_in_content && $meta_is_checked) {
                $pages[$id] = array(
                    'post_type' => $post_type,
                    'title' => get_the_title(),
                    'loc' => esc_html__('button, sidebar',
                            'antique-section-search'),
                    'link' => get_permalink(),
                );
            } else if ($is_in_content && !$meta_is_checked) {
                $pages[$id] = array(
                    'post_type' => $post_type,
                    'title' => get_the_title(),
                    'loc' => esc_html__('button', 'antique-section-search'),
                    'link' => get_permalink()
                );
            } else if (!$is_in_content && $meta_is_checked) {
                $pages[$id] = array(
                    'post_type' => $post_type,
                    'title' => get_the_title(),
                    'loc' => esc_html__('sidebar', 'antique-section-search'),
                    'link' => get_permalink()
                );
            }
        }
    }

    ksort($pages);

    wp_reset_postdata();
    ?>

    <div class="antique-plugin-table-wrap">
        <p><?php
            esc_html_e('Pages and posts containing the block:',
                    'antique-section-search');
            ?></p>
        <div id="antique-plugin-toggle-table">
            <span class="show is-displayed"
                  ><?php esc_html_e('Show', 'antique-section-search'); ?></span>
            <span class="hide"
                  ><?php esc_html_e('Hide', 'antique-section-search'); ?></span>
        </div>

        <table class="antique-plugin-pages-table">
            <tr>
                <th><?php esc_html_e('ID', 'antique-section-search'); ?></th>
                <th><?php esc_html_e('type', 'antique-section-search'); ?></th>
                <th><?php esc_html_e('title', 'antique-section-search'); ?></th>
                <th><?php esc_html_e('location', 'antique-section-search'); ?></th>
                <th><?php esc_html_e('link', 'antique-section-search'); ?></th>
            </tr>
            <?php
            foreach ($pages as $ID => $page) {
                ?>
                <tr>
                    <td><?php esc_html_e($ID); ?></td>
                    <td><?php echo $page['post_type']; ?></td>
                    <td><?php echo $page['title']; ?></td>
                    <td><?php echo $page['loc']; ?></td>
                    <td><a href="<?php echo $page['link']; ?>"
                           target="_blank"><?php
                               esc_html_e('link', 'antique-section-search');
                               ?></a></td>
                </tr>
                <?php
            }
            ?>
        </table>
    </div>

    <?php
}