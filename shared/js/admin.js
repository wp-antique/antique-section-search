jQuery(document).ready(function($){
    // Color picker.
    $('.color-field').wpColorPicker();
    
    // Table that shows on which pages/posts an Antique plugin is used.
    $('#antique-plugin-toggle-table').on('click', function() {
        $('.antique-plugin-pages-table').toggleClass('is-displayed');
        $('.antique-plugin-toggle-table .show').toggleClass('is-displayed');
        $('.antique-plugin-toggle-table .hide').toggleClass('is-displayed');
    });
    
});