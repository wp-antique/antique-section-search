jQuery(document).ready(function ($) {

    var plugins = antique_plugins_collapse_options;

    for (const [name, is_closed] of Object.entries(plugins)) {
        if ($(window).width() <= 768) {
            if (is_closed) {
                $('.antique-' + name + ' .antique-block-arrow').addClass('rotate-arrow');
                $('.antique-' + name + ' .antique-block-content').addClass('hide-content');
            } else {
                $('.antique-' + name + ' .antique-block-arrow').removeClass('rotate-arrow');
                $('.antique-' + name + ' .antique-block-content').removeClass('hide-content');
            }
        }
    }

    $('.antique-block-show-content').each(function () {
        $(this).on('click', function () {
            var $block = $(this).parents('.antique-block');
            $block.find('.antique-block-arrow').toggleClass('rotate-arrow');
            $block.find('.antique-block-content').toggleClass('hide-content');
            
        });
    });

});