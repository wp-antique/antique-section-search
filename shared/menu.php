<?php

if (!function_exists('antique_plugins_admin_menu') &&
        !function_exists('antique_plugins_admin_menu_html')) {

    add_action(
            hook_name: 'admin_menu',
            callback: 'antique_plugins_admin_menu'
    );

    function antique_plugins_admin_menu() {

        add_menu_page(
                page_title: __('Antique Plugins', 'antique-section-search'),
                menu_title: __('Antique Plugins', 'antique-section-search'),
                capability: 'manage_options',
                menu_slug: 'antique_plugins',
                callback: 'antique_plugins_admin_menu_html',
        );
    }

    function antique_plugins_admin_menu_html() {
        
    }

}